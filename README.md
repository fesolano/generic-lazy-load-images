# Implementation of lazy load in images.

This solution explains how to implement lazy load in your images.

## Getting Started

###Clone the respository

```sh
git clone https://fesolano@bitbucket.org/fesolano/generic-lazy-load-images.git
```
- Copy the lazyLoad.js file (find it here: /src/js/lazyload.js) in the js folder of your solution.
- Copy the noScript.css file (find it here: /src/css/noScript.css) in the js folder of your solution.
- Link the lazyLoad.js and noScript.css files to your HTML.
- Open any example of the cloned folder.

###Create the a JavaScript file

Create a JavaScript file that contains the next code

```javascript
var options = {
    lowQualityMargin: "150px",
    highQualityMargin: "50px",
    root: null
};
var lazy = LazyLoad(options);
```

####Required properties                                    
- root -> This is the element that is used as the viewport for checking the visibility of the target
- highQualityMargin -> This is the number of pixels that browser used before starting to download the high-quality image.

####Not Required properties                                    
- lowQualityMargin -> This is the number of pixels that browser used before starting to download the low-quality image.
    - This property is optional because you can either have a low-quality image or not have one.

Change the values of lowQualityMargin or highQualityMargin if you want to grow or shrink the root element bounding box.

###Add attributes for all images

####Required Attributes                                    
- class="lazyLoadImage" -> represents the group of images to apply the lazy load functionality
- data-src="" -> Represents the URL of the high-quality image.
- src="" -> This value is the URL the browser download, you could put a Base64 Encode of 1x1px Transparent GIF or could be in blank

####Not Required Attributes                                    
- data-lowquality -> Represents the URL of the low-quality image.
    - This property is optional because you can either have a low-quality image or not have one.
- data-srcset=""  -> This represents the URL of the image you will use in different situations depending of the resolution of your screen.
    - This property is optional because you can either have a group of images with different resolutions or not have it at all.
    - If you use this attribute, it is required to use the srcset attribute.
    - Also, if you use this, is required to use src and data-src attributes as a fallback.
- srcset="" -> This value is the URLs that the browser download depending of the resolution.

```sh
<img class="lazyLoadImage" srcset="" src=""
data-lowquality="https://nhs-dynamic.secure.footprint.net/Images/Homes/Sharp18232/24015922-170926.jpg?maxwidth=10&format=jpg&progressive=true"
data-srcset="https://nhs-dynamic.secure.footprint.net/Images/Homes/Sharp18232/24015922-170926.jpg?maxwidth=414 800w, https://nhs-dynamic.secure.footprint.net/Images/Homes/Sharp18232/24015922-170926.jpg?maxwidth=736 1500w"
data-src="https://nhs-dynamic.secure.footprint.net/Images/Homes/Sharp18232/24015922-170926.jpg?maxwidth=500&maxheight=400&format=jpg&progressive=true">
```

###Add <noscript> tag

####For images
For all image add a <noscript> tag as fallback, if JavaScript is disabled.

```sh
<noscript>
    <img class="lazyLoadImage"
    src="https://nhs-dynamic.secure.footprint.net/Images/Homes/Sharp18232/24015922-170926.jpg?maxwidth=500&maxheight=400&format=jpg&progressive=true">
</noscript>
```

####For CSS 
Add a <noscript> before end of <head> and link the noScript.css, because the images with lazyload can't be loaded and they show in the viewport.
```sh
<head>
    <link rel="stylesheet" href="css/yourStyles.css">
    <noscript>
        <link rel="stylesheet" href="css/noScript.css">
    </noscript>
</head>
```

###Link scripts
Link lazyLoad.js before your another files.

```sh
<script src="js/lazyLoad.js"></script>
<script src="js/yourFileName.js"></script>
```

Note: The files style.css, mainIndex.js, mainExample2.js, mainExample3.js, and mainExample4.js in the folder that you clone, are demonstrative, you can ignore those files.
function changeCode(event){
    var elementCode = event.target.getAttribute("data-code");
    if(elementCode){
        var showCode = document.querySelector("#wrapper");
        showCode.querySelector(".showCode").classList.remove("showCode");
        showCode.children[elementCode].classList.add("showCode");
    }
}

var menuCode = document.querySelector("#code");
if(menuCode){
    menuCode.addEventListener("click", changeCode);
}
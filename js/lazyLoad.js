var LazyLoad = function (options) {
    var root = window;
    var listingContainer = document.body;
    var rootMarginObserver;
    var viewportMarginObserver;

    var removeClass = function () {
        this.classList.remove("lazyLoadImage", "nhs_lazyLoadSize");
    };

    function lazyLoadWithPolyfill() {
        var lazyImages = getAllImages();

        setTimeout(function () {
            for (var index = 0; index < lazyImages.length; index++) {
                var lazyImage = lazyImages[index];
                if (lazyImage.offsetParent === null && lazyImage.className.indexOf("ie-lazyLoad") === -1)
                    return;

                if (!lazyImage.dataset) return;

                var boundingClient = lazyImage.getBoundingClientRect();

                if (((boundingClient.top <= root.innerHeight + options.highQualityMargin && boundingClient.bottom + options.highQualityMargin >= 0) &&
                    getComputedStyle(lazyImage).display !== "none") || lazyImage.className.indexOf("ie-lazyLoad") > -1) {

                    lazyImages = lazyImages.filter(function (image) {
                        return image !== lazyImage;
                    });
                }

                if ((boundingClient.top < root.innerHeight) || lazyImage.className.indexOf("ie-lazyLoad") > -1) {
                    var srcsetImage = lazyImage.dataset.srcset;

                    lazyImage.onload = removeClass;

                    if (srcsetImage !== "" && srcsetImage){
                        var srcsetValues = srcsetImage.split(" ");
                        if(srcsetValues[0]){
                            lazyImage.src = srcsetValues[0];
                            lazyImage.srcset = srcsetImage;
                        }
                    }
                    else
                        lazyImage.src = lazyImage.dataset.src;

                    lazyImages = lazyImages.filter(function (image) {
                        return image !== lazyImage;
                    });
                }
            }

        }, 200);
    }

    function rootMarginIntersectionObserver() {
        var lazyImages = getImages(false);
        var config = {
            rootMargin: options.lowQualityMargin,
            root: options.root
        };

        rootMarginObserver = new IntersectionObserver(function (entries) {

            for(var index = 0; index < entries.length; index++){
                var entry = entries[index];
                if (entry.intersectionRatio > 0 && entry) {
                    var lazyImage = entry.target;
                    lazyImage.src = lazyImage.dataset.lowquality;
                    rootMarginObserver.unobserve(lazyImage);
                    viewportMarginObserver.observe(lazyImage);
                }
            }
        },
            config);

        for(var index = 0; index < lazyImages.length; index++){
            var lazyImage = lazyImages[index];
            rootMarginObserver.observe(lazyImage);
        }
    }

    function viewportIntersectionObserver() {
        var lazyImages = getImages(true);
        var config = {
            rootMargin: options.highQualityMargin,
            root: options.root
        };
        viewportMarginObserver = new IntersectionObserver(function (entries) {

            for(var index = 0; index < entries.length; index++){
                var entry = entries[index];
                if (entry.intersectionRatio > 0 && entry.isIntersecting) {
                    var lazyImage = entry.target;
                    var srcsetImage = lazyImage.dataset.srcset;

                    lazyImage.onload = removeClass;

                    if (srcsetImage !== "" && srcsetImage) {
                        var srcsetValues = srcsetImage.split(" ");
                        if(srcsetValues[0]){
                            lazyImage.src = srcsetValues[0];
                            lazyImage.srcset = srcsetImage;
                        }
                    } else if (lazyImage.dataset.src) {
                        lazyImage.src = lazyImage.dataset.src;
                    }
                    viewportMarginObserver.unobserve(lazyImage);
                }
            }
        },
        config);

        for(var index = 0; index < lazyImages.length; index++){
            var lazyImage = lazyImages[index];
            viewportMarginObserver.observe(lazyImage);
        }
    }


    function getImages(isViewport) {
        return isViewport ? [].slice.call(root.document.querySelectorAll(".lazyLoadImage:not([data-lowquality])")) : [].slice.call(root.document.querySelectorAll(".lazyLoadImage[data-lowquality]"));
    }

    function getAllImages() {
        return [].slice.call(root.document.querySelectorAll(".lazyLoadImage"));
    }

    function LoadImages() {

        if (root.IntersectionObserver && root.IntersectionObserver.toString().indexOf('[native code]') > -1) {
            viewportIntersectionObserver();
            rootMarginIntersectionObserver();
        } else {
            root.addEventListener("resize", lazyLoadWithPolyfill);
            root.addEventListener("scroll", lazyLoadWithPolyfill);
            root.addEventListener("orientationchange", lazyLoadWithPolyfill);

            lazyLoadWithPolyfill();
        }
    }

    var init = function () {
        if (listingContainer) {
            var observer = new MutationObserver(function () {
                LoadImages();
            });

            observer.observe(listingContainer, {
                attributes: true,
                childList: true,
                characterData: true
            });
        }
    }

    init();
};